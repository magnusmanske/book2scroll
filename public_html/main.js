/* book2scroll - a fluent viewer for wikisource content
   © 2010 by Magnus Manske
   Released under GPL V3
*/

var fileserver = 'commons.wikimedia.org' ;
var api = 'query.php?callback=?' ;
var isiPad = navigator.userAgent.match(/iPad/i) != null;
var isiPod = navigator.userAgent.match(/iPod/i) != null;
var isiPhone = navigator.userAgent.match(/iPhone/i) != null;
var isiOS = isiPad || isiPod || isiPhone ;

var header_classes = [ 'generic_header_class' , 'quality0' , 'quality1' , 'quality2' , 'quality3' , 'quality4' , 'metadata' ] ;
var header_ids = [ 'pagequality' , 'bstand' , 'zitierhilfe' ] ;

var ts_base = 'http://toolserver.org/' ;
var lang ;
var page_lang = 'Page' ; // Default
var server ;
var textserver ;
var ns_page = 104 ;
var numberlength ;
var source_file ;
var page_base ;
var first_page ;
var last_page ;
var pages = new Array () ;
var first_page_loaded = true ;
var page_is_loading = 0 ;
var original_image_width ;
var original_image_height ;
var universal_image_thumbnail_url ;
var universal_thumb_width ;
var universal_thumb_height ;
var highlight_string = '' ;
var initial_page ;
var testing ;
var last_page_seen = 1 ;
//var myScroll ;

var locales = new Object () ;
locales['en'] = [ 104 , 'Page' , 'English' ] ;
locales['fr'] = [ 104 , 'Page' , 'French' ] ;
locales['de'] = [ 102 , 'Seite' , 'German' ] ;
locales['it'] = [ 108 , 'Pagina' , 'Italian' ] ;
locales['la'] = [ 104 , 'Pagina' , 'Latin' ] ;
locales['pt'] = [ 106 , 'Página' , 'Portuguese' ] ;
locales['be'] = [ 104 , 'Старонка' , 'Belarusian' ] ;
locales['bn'] = [ 104 , 'Page' , 'Bengali' ] ;
locales['ru'] = [ 104 , 'страница' , 'русский' ] ;
locales['ta'] = [ 250 , 'பக்கம்' , 'தமிழ்' ] ;
locales['cs'] = [ 250 , 'Stránka', 'Czech' ] ;

$(document).ready ( function () {
	if ( isiOS ) {
		document.body.setAttribute("orient", 'landscape');
		window.scrollTo(0, 1);
		$('#clear_search').show();
		toggle_header_hideable() ;
//		myScroll = new iScroll('body');
	}
	
	// Get URL parameters
	testing = 1 * getURLParameter ( 'test' , 0 ) ;
	lang = '' + getURLParameter ( 'lang' , '' ) ;
	
	if ( 1 * getURLParameter ( 'show_headers' , 0 ) ) $('#show_headers').attr('checked', true);
	if ( 1 * getURLParameter ( 'hide_text' , 0 ) ) $('#show_text').attr('checked', false);
	if ( 1 * getURLParameter ( 'hide_image' , 0 ) ) $('#show_image').attr('checked', false);

	
	if ( locales[lang] ) {
		ns_page = locales[lang][0] ;
		page_lang = locales[lang][1] ;
	} else {
		show_available_books() ;
		$('#bookbox').html ( "<h1>Welcome to book2scroll!</h1>Please begin by selecting a language fron the list on the left." ) ;
//		alert ( 'Language ' + lang + ' not supported - please bother the software author! [[en:user:Magnus Manske]]' ) ;
		return ;
	}
	
	source_file = '' + getURLParameter ( 'file' , '' ) ; // 'Dictionary_of_National_Biography_volume_11.djvu' ) ;
	if ( source_file == '' ) {
		show_available_books () ;
		return ;
	}
	
	init_vars () ;
} ) ;


function get_max_page_number () {
	$.getJSON ( api , {
		query:'max_page_number',
		lang:lang,
		title:source_file,
		ns:ns_page
	} , function ( data ) {
		last_page = 1 * data.pages ;
		numberlength = 1 * data.numlen ;
		initial_view () ;
	} ) ;
}


function init_vars () {
	source_file = source_file.split(' ').join('_') ;
	source_file = source_file.split('+').join('_') ;
	page_base = source_file ; // Can that ever be different?
	server = '//'+lang+'.wikisource.org' ;
	textserver = '//'+lang+'.wikisource.org' ;
	first_page = 1 ;
	last_page = getURLParameter ( 'pages' , 'auto' ) ; 
	initial_page = 1 * getURLParameter ( 'startpage' , 1 ) ;

	detectFileServer ( function () {
		if ( last_page == 'auto' ) {
			get_max_page_number () ;
		} else {
			numberlength = 1 * getURLParameter ( 'numlen' , 0 ) ;
			last_page = 1 * last_page ;
			initial_view () ;
		}
	} ) ;
}

function show_available_books () {
	var h = '<div id="langbox" style="border:1px solid #EEEEEE;position:absolute;left:2px;top:2px;bottom:2px;padding-right:5px;width:auto">' ;
	h += "<format class='form-inline'>" ;
	$.each ( locales , function ( l , v ) {
		h += "<div><label class='checkbox'>" ;
		h += '<input type="radio" name="booklang" value="'+l+'" id="bl_'+l+'"' ;
		if ( l == lang ) h += ' checked' ;
		h += ' onchange="load_books_for_language(\''+l+'\');"' ;
		h += '/> '+v[2]+'</label></div>' ;
	} ) ;
	h += '</div>' ;
	h += '<div id="bookbox" style="border:1px solid #EEEEEE;position:absolute;left:200px;top:2px;bottom:2px;right:2px;overflow-y:auto">' ;
	h += "</form>" ;
	h += '</div>' ;
	$('#header').hide();
	$('#body').html ( h ) ;
	$('#body').css({top:'2px',left:'2px',right:'2px',width:'auto'});
	$('#body').show();
	
	var p = $('#langbox').offset() ;
	p = p.left + $('#langbox').width() + 5 ;
	$('#bookbox').css({left:p+'px'});

	
	var l = lang ;
	lang = '' ;
	load_books_for_language ( l ) ;
}

function quote4attr ( s ) {
	return s.replace ( /"/g , '&quot;' ) ;
}

function ucfirst ( s ) {
	return s.charAt(0).toUpperCase() + s.slice(1);
}

function load_books_for_language ( l ) {
	if ( l == lang ) return ;
	lang = l ;
	$('#bookbox').html ( '<i>loading books from ' + l + '.wikisource ...</i>' ) ;
	$('#loading').show() ;
	$.getJSON ( api , {
		query:'books_for_language',
		lang:l,
		ns:locales[l][0]
	} , function ( data ) {
		var h = "<table class='table table-condensed table-striped'>" ; // border=1 cellpadding=2 cellspacing=0
		h += "<thead><tr><th>Pages</th><th>Info</th><th>File / Title</th></tr></thead><tbody>" ;
		$.each ( data , function ( book , meta ) {
			var book_add = '' ;
			var book_nice = book.split('_').join(' ') ;
			book_nice = book_nice.split('.');
			book_nice.pop();
			book_nice = book_nice.join('.');
			
			if ( undefined !== meta.nice && book_nice.replace(/[+-.]/g,'').toLowerCase() != meta.nice.replace(/[+-.]/g,'').toLowerCase() ) {
				if ( book_nice.substr( 0 , meta.nice.length ) == meta.nice ) {
					book_nice = $.trim ( book_nice.substr ( meta.nice.length ) ) ;
					book_nice = ucfirst($.trim(book_nice.replace(/^\((.+)\)$/g,'$1').replace(/^[,-]+\s*/g,'')));
				}
				book_add = ' <small>(' + book_nice + ')</small>' ;
				book_nice = meta.nice ;
			}
			
			
			h += "<tr book=\"" + escape(book) + "\">" ;
			h += "<td style='text-align:right;font-family:Courier'>" + meta.pn + "</td>" ;
			h += "<td style='text-align:center'><a href='#' class='showinfolink'><i class='icon-info-sign'/></a></td>" ;
			h += '<td><a href="#" onclick="load_new_book(\''+quote4attr(book)+'\',\''+l+'\');return false;">' + book_nice + '</a>' + book_add + '<div class="info_loading"></div><div class="bookinfo"></div></td>' ;
			h += "</tr>" ;
		} ) ;
		h += "</tbody></table>" ;
		$('#bookbox').html ( h ) ;
		$('#loading').hide() ;
		$('#bookbox a.showinfolink').click ( function () {
			var tr = $($(this).parents('tr').get(0)) ;
			var b = unescape ( tr.attr('book') ) ;
			var bi = $(tr.find('div.bookinfo')) ;
			gbi = bi ; // TESTING
			if ( bi.is(":visible") ) bi.hide() ; // Is visible, 
			else if ( bi.text() != '' ) bi.show() ; // Already loaded, just show
			else { // Need to load info
				$(tr.find('span.info_loading')).html('<i>Loading book info...</i>').show() ;
				$.getJSON ( '//'+lang+'.wikisource.org/w/api.php?callback=?' , {
					action:'parse',
					page:'Index:'+b,
					prop:'text',
					format:'json'
				} , function ( d ) {
					var h = d.parse.text['*'] ;
					bi.html(h) ;
					bi.find('.Top_icon_raw').remove();
					bi.find('.ombox').remove();
					bi.find('#remarks').remove();
					bi.html ( $(bi.find('table table td').get(0)).html() ) ;
					bi.find('a').each ( function () {
						var o = $(this) ;
						o.attr ( { target:'_blank',href:'//'+lang+'.wikisource.org'+o.attr('href') } ) ;
					} ) ;
					$(tr.find('span.info_loading')).hide() ;
					bi.show() ;
				} ) ;
			}
			return false ;
		} ) ;
	} ) ;
}

function load_new_book ( book , l ) {
	lang = l ;
	source_file = book ;
	$('#body').hide();
	$('#body').html ( '' ) ;
	$('#body').css({top:'42px',left:'0px',right:'0px',width:'500px'});
	$('#header').show();
	init_vars () ;
	return false ;
}

function detectFileServer ( callback ) {
	fileserver = 'commons.wikimedia.org' ;
	$.getJSON ( '//'+fileserver+'/w/api.php?callback=?' , {
		action:'query',
		titles:'File:'+source_file,
		prop:'imageinfo',
		iiprop:'url|size|dimensions|sha1|mime|metadata|archivename|bitdepth',
		format:'json'
	} , function ( data ) {
		var n ;
		$.each ( data.query.pages , function ( k , v ) { n = k ; } ) ;
		if ( n == -1 ) fileserver = lang+'.wikisource.org' ;
		callback() ;
	} ) ;
}

function initial_view () {
	var title = source_file ;
	title = '' + title.match ( /^(.+)\.[^.]+$/ )[1] ;
	title = title.split('_').join(' ') ;
	$('#title').text ( title ) ;

	$('#last_page_number').html ( last_page - first_page + 1 ) ;
	var sel = '<select id="page_select" class="input-small">' ;
	for ( var p = first_page ; p <= last_page ; p++ ) {
		$('#body').append ( '<div class="page" id="page_' + p + '" style="width:100%;background:#EEEEEE" onclick="load_page('+p+',0);return false;">(page ' + p + ' not loaded)</div>' ) ;
		sel += '<option value="' + p + '">' + p + '</option>' ;
	}
	sel += '</select>' ;
	$('#selbox').html ( sel ) ;
	$('#page_select').change ( function () {
		var page = $('#page_select').val() ;
		jump_to_page ( page ) ;
		load_page ( page , 0 ) ;
		update_page_counter () ;
	} ) ;
	
	
	// Get image properties and load first page
	$.getJSON ( '//'+fileserver+'/w/api.php?callback=?' , {
		action:'query',
		titles:'File:'+source_file,
		prop:'imageinfo',
		iiprop:'url|size|dimensions|sha1|mime|metadata|archivename|bitdepth',
		format:'json'
	} , function ( data ) {
		var n ;
		$.each ( data.query.pages , function ( k , v ) {
			n = v ;
		} ) ;
		original_image_width = n.imageinfo[0].width ;
		original_image_height = n.imageinfo[0].height ; 
		
		show_headers ();
		load_page ( initial_page , 0 ) ;
		update_permalink ( initial_page ) ;
	} ) ;
}

function getURLParameter(name,def) {
    return unescape(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,def])[1]
    );
}

function adjust_body_width ( nw ) {
	var bw = $('#body').width() ;
//	if ( bw >= nw ) return ;
	var rbw = $('#container').width() ;
	var l = Math.floor((rbw-nw)/2) ;
	$('#body').width(nw) ;
	$('#body').css({left:l}) ;

	if ( $('#searchmarkers').is(':visible') ) {
		var p = $('#body').offset() ;
		p = p.left + $('#body').width() ;
		$('#searchmarkers').css({left:p+10});
	}
	
}

function fix_ids () {
	$.each ( header_ids , function ( k , v ) {
		$('#'+v).each ( function ( k1 , v1 ) {
			$(v1).attr('id','');
			$(v1).addClass ( 'generic_header_class' ) ;
		} ) ;
	} ) ;
}

function show_page ( page , level ) {
	if ( pages[page].shown ) return ;
	if ( !pages[page].text_loaded ) return ;
	if ( !pages[page].image_loaded ) return ;
	pages[page].shown = true ;

	var pagenum = get_pagenum ( page ) ;
	var h = '' ;
	h += '<div class="page-number"><b>' + page + '</b>' ;
	h += '<br/><a href="'+pages[page].server+'/wiki/' + pages[page].page_title + '">view</a>' ;
	h += '<br/><a href="'+pages[page].server+'/w/index.php?action=edit&title=' + pages[page].page_title + '">edit</a>' ;
	h += '</div>' ;
	h += '<div class="page-text" style="width:' + pages[page].image_width + 'px">' + pages[page].text + '</div>' ; // ;max-height:' + pages[page].image_height + 'px
	h += '<div class="page-vsep" style="height:' + pages[page].image_height + 'px"></div>' ;
	h += '<div class="page-image"><img src="' + pages[page].image_url + '" /></div>' ; // onload="$(this).parent().css(\{background:\"none\"\});" 
	
	$('#page_'+page).height ( 'auto' ) ;
//	$('#page_'+page).height ( pages[page].image_height ) ;
	$('#page_'+page).html ( h ) ;
	$('#page_'+page).attr('onclick','');
	fix_ids () ;
	
	// Fixing intrawiki transclusion
	if ( pages[page].lang == lang ) {
		var span ;
		var is_trans = false ;
		$('#page_'+page+' .iwpage').each ( function () {
			span = $(this) ;
			is_trans = true ; 
		} ) ;
		if ( is_trans ) {
			$('#page_'+page+' .pagetext').css('background','#FFEEEE') ;
			var n = span.attr('title').split('|') ;
			var alt_lang = n.shift() ;
			var alt_page = n.shift() ;
			if ( locales[alt_lang] ) {
				pages[page].shown = false ;
				pages[page].text_loaded = false ;
				load_page_text ( page , level , alt_lang , alt_page ) ;
			}
		}
	}
	
	if ( highlight_string != '' ) {
		$('#page_'+page).highlight ( highlight_string , 1 , 'highlight' ) ;
	}
	
	show_headers ( '#page_'+page ) ;
	$('#page_'+page).css({background:'none'});

	$('#page_'+page+' a').each ( function (k,v) {
		if ( v.href.substr(0,ts_base.length) == ts_base ) {
			v.href = server + '/' + v.href.substr(ts_base.length) ;
		}
	} ) ;
	
	
//	adjust_body_width ( pages[page].image_width * 2 + 50 ) ;
	
	if ( first_page_loaded ) {
		$('#body').show() ;
		first_page_loaded = false ;
		for ( var p = first_page ; p <= last_page ; p++ ) {
			if ( p == page ) continue ;
			$('#page_'+p).height(pages[page].image_height) ;
		}
		jump_to_page ( page ) ;
		$('#body').scroll ( on_body_scroll ) ;
	}
	
	if ( level == 0 ) {
		update_page_counter() ;
		load_page ( page + 1 , 1 ) ;
	} else {
	}
	
	page_is_loading-- ;
	if ( page_is_loading == 0 ) $('#loading').hide() ;
}

function jump_to_page ( page ) {
	var t = $('#page_'+page).position().top * 1 + $('#body').scrollTop() * 1 ;
	$('#body').scrollTop(t) ;
}

function on_body_scroll () {
	if ( page_is_loading > 0 ) return ; // Overload preventer
	update_page_counter () ;
	var h = $('#body').height() ;
	var p_start = $('#page_select').val();
	if ( !p_start ) p_start = first_page ;

	p_start++ ;
	do {
		p_start-- ;
		var pt = $('#page_'+p_start).position()['top'] ;
		var pb = pt+$('#page_'+p_start).height() ;
	} while ( pb >= 0 && p_start > first_page ) ;
	
	for ( var p = p_start ; p <= last_page ; p++ ) {
		var pt = $('#page_'+p).position()['top'] ;
		var pb = pt+$('#page_'+p).height() ;
		if ( pb < 0 ) continue ;
		if ( pt > h ) break; ;

		if ( pages[p] ) {
			if ( pages[p+1] ) load_page ( p+2 , 1 ) ;
			else load_page ( p+1 , 1 ) ;
		} else {
			load_page ( p , 0 ) ;
		}
		break ;
	}
}

function update_page_counter () {
	var h = $('#body').height() ;
	for ( var p = first_page ; p <= last_page ; p++ ) {
		var pt = $('#page_'+p).position()['top'] ;
		var pb = pt+$('#page_'+p).height() ;
		if ( pb < 0 ) continue ;
		if ( pt > h ) break ;
		$('#page_select').val ( p ) ;
		break ;
	}
	update_permalink ( p ) ;
}

function update_permalink ( page ) {
	last_page_seen = page ;
	var url = '/book2scroll/' ;
	url += '?lang=' + lang ;
	url += '&file=' + escape ( source_file ) ;
	url += '&pages=' + last_page ;
	if ( numberlength != 0 ) url += '&numlen=' + numberlength ;
	url += '&startpage=' + page ;
	if ( $('#show_headers').is(':checked') ) url += '&show_headers=1' ;
	if ( !$('#show_text').is(':checked') ) url += '&hide_text=1' ;
	if ( !$('#show_image').is(':checked') ) url += '&hide_image=1' ;
	$('#permalink').attr('href',url);
}

function load_page ( page , level ) {
	if ( page > last_page ) return ;
	if ( pages[page] ) return ;
	page_is_loading++ ;
	if ( page_is_loading == 1 ) $('#loading').show() ;
	pages[page] = new Object ;
	pages[page].exists = true ;
	load_image_url ( page , level ) ;
	load_page_text ( page , level ) ;
}

function get_pagenum ( page ) {
	var ret = '' + page ;
	while ( ret.length < numberlength ) ret = '0' + ret ;
	return ret ;
}

function load_page_text ( page , level , alt_lang , alt_page ) {
	var pagenum = get_pagenum ( page ) ;
	var srv = textserver ;
	var pt = page_lang + ':' + page_base + '/' + pagenum ;
	pages[page].lang = lang ;
	if ( alt_lang ) {
		srv = 'http://' + alt_lang + '.wikisource.org' ;
		if ( alt_page ) pt = locales[alt_lang][1] + ':' + alt_page ;
		pages[page].lang = alt_lang ;
	}
	pages[page].page_title = pt ;
	pages[page].server = srv ;
//	var url = srv + '/w/api.php?action=parse&format=json&page=' + pt + '&callback=?' ;
	$.getJSON ( srv + '/w/api.php?callback=?' , {
		action:'parse',
		page:pt,
		format:'json'
	} ,function ( data ) {
		if ( undefined === data.parse ) return ;
		pages[page].text = data.parse.text['*'] ;
		pages[page].text_loaded = 1 ;
		show_page ( page , level ) ;
	} ) ;
}

function fix_thumbnail_url ( page ) {
	var filetype = '.' + source_file.split('.').pop() ;
	var s = filetype + '/page1-' ;
	var t = filetype + '/page' + page + '-' ;
	var ret = universal_image_thumbnail_url.split(s) ;
	ret = ret.join(t) ;
	return ret ;
}

function load_image_url ( page , level ) {

	if ( universal_image_thumbnail_url ) {
		pages[page].image_loaded = 1 ;
		pages[page].image_url = fix_thumbnail_url ( page ) ;
		pages[page].image_width = universal_thumb_width ;
		pages[page].image_height = universal_thumb_height ;
		show_page ( page , level ) ;
		return ;
	}

	var want_height = $('#body').height() 
	var want_width = want_height * original_image_width / original_image_height ;
	var size = Math.floor ( want_width / 100 ) * 100 ;
	while ( size * 2 + 50 > $('#container').width() ) size -= 100 ;

//	var url = 'http://commons.wikimedia.org/w/api.php?action=query&titles=File:' + source_file ;
//	url += '&prop=imageinfo&iiurlwidth=' + size ;
//	url += '&iiprop=timestamp|user|comment|url|size|dimensions|sha1|mime|metadata|archivename|bitdepth&format=json&callback=?' ;
	$.getJSON ( '//'+fileserver+'/w/api.php?callback=?' , {
		action:'query',
		titles:'File:'+source_file,
		prop:'imageinfo',
		iiurlwidth:size,
		iiprop:'timestamp|user|comment|url|size|dimensions|sha1|mime|metadata|archivename|bitdepth',
		format:'json'
	} ,function ( data ) {
		var n ;
		$.each ( data.query.pages , function ( k , v ) {
			n = v ;
		} ) ;
		
		universal_image_thumbnail_url = n.imageinfo[0].thumburl ;//.replace(/^http:/,'') ;
		universal_thumb_width = n.imageinfo[0].thumbwidth ;
		universal_thumb_height = n.imageinfo[0].thumbheight ;
		
		pages[page].image_loaded = 1 ;
		pages[page].image_url = fix_thumbnail_url ( page ) ;
		pages[page].image_width = universal_thumb_width ;
		pages[page].image_height = universal_thumb_height ;
		show_page ( page , level ) ;
	} ) ;
}

function show_headers ( root ) {
	var do_show = $('#show_headers').is(':checked') ;
	$.each ( header_classes , function ( k , classname ) {
		if ( do_show ) $(root+' .'+classname).show() ;
		else $(root+' .'+classname).hide() ;
	} ) ;
	
	if ( $('#show_text').is(':checked') ) $(root+' .page-text').show() ;
	else $(root+' .page-text').hide() ;

	if ( $('#show_image').is(':checked') ) $(root+' .page-image').show() ;
	else $(root+' .page-image').hide() ;
	
	if ( $('#show_image').is(':checked') && $('#show_text').is(':checked') ) {
		$(root+' .page-vsep').show() ;
		adjust_body_width ( universal_thumb_width * 2 + 50 ) ;
	} else {
		$(root+' .page-vsep').hide() ;
		adjust_body_width ( universal_thumb_width + 50 ) ;
	}

	update_permalink ( last_page_seen ) ;
}

function toggle_headers () {
	show_headers ( '#body' ) ;
}

function toggle_text () {
	show_headers ( '#body' ) ;
}

function toggle_image () {
	show_headers ( '#body' ) ;
}

function do_search () {
	remove_highlight ( '#body' , 'highlight' ) ;
	$('#searchmarkers').hide() ;
	var query = $('#search').val() ;
//	var url = 'http://toolserver.org/~magnus/book2scroll/booksearch.php?server=' + escape(textserver) + '&book=' + escape(source_file) + '&query=' + escape(query) + '&ns=' + ns_page + '&callback=?' ;
	$('#loading').show() ;
	$.getJSON ( api , {
		query:'booksearch',
		searchquery:query,
		server:textserver,
		ns:ns_page,
		title:source_file
	} , function ( data ) {
		var rp = new Array () ;
		$.each ( data , function ( k , v ) {
			rp.push ( k ) ;
		} ) ;
		rp = rp.sort(sortNumber) ;
		var h = '<table cellpadding="2px"><thead><b>Search results <sup> [<a href="#" onclick="clear_search();return false;">X</a>]</sup></b></thead><tbody>' ;
		$.each ( rp , function ( k , page ) {
			h += '<tr><td nowrap><a href="#" onclick="jump_and_show_page('+page+');return false">Page ' + page + '</a></td></tr>' ;
		} ) ;
		h += '</tbody></table>' ;
		if ( rp.length == 0 ) {
			h += '<i>No hits.</i>' ;
		} else {
			$('#body').highlight ( query , 1 , 'highlight' ) ;
			highlight_string = query ;
		}
//		$('#searchresults').css('max-height',$('#body').height()) ;
//		$('#searchresults').html ( h ) ;
//		$('#searchresults').show() ;

		// Draw result markers
		var h = $('#body').height() ;
		var markers = '' ;
		$.each ( rp , function ( k , page ) {
			var y = Math.floor ( page * h / ( last_page - first_page + 1 ) ) ;
			markers += '<div class="resmark" style="top:' + y + 'px" onclick="jump_and_show_page('+page+');return false;" title="'+page+'"></div>' ;
		} ) ;
		var p = $('#body').offset() ;
		p = p.left + $('#body').width() ;
		$('#searchmarkers').css({left:p+10});
		$('#searchmarkers').html(markers);
		$('#searchmarkers').show() ;

		$('#loading').hide() ;
	} ) ;
	return false ;
}

function sortNumber(a,b) {
	return a - b;
}

function jump_and_show_page ( page ) {
	load_page(page+1,1);
	jump_to_page ( page ) ;
	load_page(page,0);
}

function remove_highlight ( id , classname ) {
	if ( highlight_string == '' ) return ;
	highlight_string = '' ;
	$(id+' .'+classname).each ( function (k,v) {
		$(v).replaceWith ( $(v).html() ) ;
	} ) ;
}

function clear_search () {
	remove_highlight ( '#body' , 'highlight' ) ;
	$('#searchresults').hide() ;
	$('#searchmarkers').hide() ;
}

function toggle_header_hideable() {
	if ( $('#pagenav').is(':visible') ) {
		$('.header-hideable').fadeOut('slow') ;
		$('#clickhint').fadeIn('slow');
	} else {
		$('.header-hideable').fadeIn('slow') ;
		$('#clickhint').fadeOut('slow');
	}
}

// iOS hack

 function BlockMove(event) {
  // Tell Safari not to move the window.
  event.preventDefault() ;
 }


// JQUERY EXTENSIONS

jQuery.fn.extend({
    highlight: function(search, insensitive, hls_class){
      var regex = new RegExp("(<[^>]*>)|(\\b"+ search.replace(/([-.*+?^${}()|[\]\/\\])/g,"\\$1") +")", insensitive ? "ig" : "g");
      return this.html(this.html().replace(regex, function(a, b, c){
        return (a.charAt(0) == "<") ? a : "<span class=\""+ hls_class +"\">" + c + "</span>";
      }));
    }
  });

