<?PHP

include ( 'php/common.php' ) ;

$query = get_request('query','') ;
$callback = get_request('callback','') ;
$ns = get_request('ns',0) ;
$language = get_request('lang','en') ;
$title = utf8_encode ( get_request('title','') ) ;
$project = 'wikisource' ;
$test = isset ( $_REQUEST['test'] ) ;

if ( $test ) print header('Content-type: text/plain');
else print header('Content-type: application/json');

$db = openDB ( $language , $project ) ;
make_db_safe ( $ns ) ;

$output = array('error'=>'Wrong or missing query') ;
if ( $query == 'max_page_number' ) {

	$sql = "SELECT page_title FROM page WHERE page_namespace=$ns AND page_title LIKE \"" . get_db_safe($title) . "/%\"" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$max = 1 ;
	$nl = 0 ;
	while ( $o = $result->fetch_object() ) {
		$s = $o->page_title ;
		$s = substr ( $s , strlen ( $title ) + 1 ) ;
		if ( substr ( $s , 0 , 1 ) == '0' and $s != '0' ) $nl = strlen ( $s ) ;
		$s = $s * 1 ;
		if ( $max < $s ) $max = $s ;
	}
	$output = array ( 'pages' => $max , 'numlen' => $nl  ) ;
	
} else if ( $query == 'books_for_language' ) {

	$output = array () ;
	$sql = "select distinct(left(page_title,length(page_title)-1-length(SUBSTRING_INDEX(page_title,'/',-1)))) AS base_title,count(*) AS cnt FROM page WHERE page_namespace=$ns and (lower(page_title) like '%.djvu/%' or lower(page_title) like '%.pdf/%') group by base_title" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while ( $o = $result->fetch_object() ) {
		$output[$o->base_title]['pn'] = $o->cnt ;
	}
	ksort ( $output ) ;
	
	if ( $language == 'en' ) {
		$files = array() ;
		foreach ( $output AS $file => $v ) $files[] = get_db_safe ( $file ) ;
		$sql = "select page_title,pl_title from page,pagelinks where page_id=pl_from AND pl_namespace=0 AND page_title IN ('" . implode("','",$files) . "') AND page_namespace=106" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while ( $o = $result->fetch_object() ) {
			if ( isset ( $output[$o->page_title] ) and !isset($output[$o->page_title]['nice']) ) 
				$output[$o->page_title]['nice'] = str_replace ( '_' , ' ' , $o->pl_title ) ;
		}
	}
	
} else if ( $query == 'booksearch' ) {

	$server = get_request('server','') ;
	$searchquery = str_replace ( '_' , ' ' , get_request('searchquery','') ) ;
	$sb = str_replace ( '-' , ' ' , $title ) ;
	$url = $server . '/w/index.php?title=Special:Search&ns' . $ns . '=1&redirs=0&advanced=1&search=' . urlencode($searchquery." '".$sb."'") . '&limit=500&offset=0' ;

	ini_set('user_agent','Magnus tools'); # Fake user agent
	$t = file_get_contents ( $url ) ;
	$t = array_pop ( explode ( 'mw-search-results' , $t , 2 ) ) ;
	$t = array_shift ( explode ( '</ul>' , $t , 2 ) ) ;
	$t = explode ( "\n" , $t ) ;
	$s = array() ;
	$rb = urlencode ( str_replace ( ' ' , '_' , $title ) ) ;
	foreach ( $t AS $l ) {
		$l = trim ( $l ) ;
		if ( substr ( $l , 0 , 4 ) != '<li>' ) continue ;
		if ( 0 == preg_match ( '/\shref="([^"]+)\/0*(\d+)"/' , $l , $match ) ) continue ;
	
		// TODO : Check if it's really the right book!
		if ( 0 == preg_match ( '/:'.$rb.'$/' , $match[1] ) ) continue ;
		if ( $test ) print $rb . "\t" . $match[1] . "\n" ;
	
		$s[$match[2]]++ ;
	}

	$output = $s ;
}

print ($callback."(".json_encode ($output).");");

?>